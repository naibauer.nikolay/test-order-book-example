﻿using System;
using OrderBookExample;

public class Program
{
    public static decimal randomDecimal(Random randomNumberGenerator)
    {
        int precision = 6;
        int scale = randomNumberGenerator.Next(2, precision);

        if (randomNumberGenerator == null)
            throw new ArgumentNullException("randomNumberGenerator");
        if (!(precision >= 1 && precision <= 28))
            throw new ArgumentOutOfRangeException("precision", precision, "Precision must be between 1 and 28.");
        if (!(scale >= 0 && scale <= precision))
            throw new ArgumentOutOfRangeException("scale", precision, "Scale must be between 0 and precision.");

        Decimal d = 0m;
        for (int i = 0; i < precision; i++)
        {
            int r = randomNumberGenerator.Next(0, 10);
            d = d * 100m + r;
        }
        for (int s = 0; s < scale; s++)
        {
            d /= 10m;
        }
        //if (randomNumberGenerator.Next(2) == 1)
        //  d = decimal.Negate(d);
        return d;
    }

    public static void Main()
    {

        OrderBook orderBook = new OrderBook(6, 6);

        int amount = 100000;

        Random rnd = new Random();

        decimal[] price = new decimal[amount];
        decimal[] size = new decimal[amount];

        for (int i = 0; i < amount; i++)
        {
            price[i] = randomDecimal(rnd);
            size[i] = randomDecimal(rnd);
        }

        long s = DateTime.Now.Ticks; 

        for (int i = 0; i < amount; i++)
            orderBook.Update(Side.Ask, price[i], size[i]);

        long e = DateTime.Now.Ticks;

        TimeSpan elapsedSpan = new TimeSpan(e - s);

        Console.WriteLine("Update seconds per 1000: " + elapsedSpan.TotalSeconds/(amount/1000));

        decimal price_t = orderBook.AskBook[orderBook.AskCounter / 2].Price;

        s = DateTime.Now.Ticks;

        Level[] topLevels = orderBook.GetTop(Side.Ask, price_t);

        Console.WriteLine("Get top by price: " + (new TimeSpan(DateTime.Now.Ticks - s)).TotalSeconds);

        s = DateTime.Now.Ticks;

        decimal size_t = orderBook.AskBook[orderBook.AskCounter / 2].CumulSize.GetValueOrDefault();

        decimal? price_cum = orderBook.GetPriceWhenCumulGreater(Side.Ask, size_t);

        Console.WriteLine(" GetPriceWhenCumulGreater " + price_cum + ": " + (new TimeSpan(DateTime.Now.Ticks - s)).TotalSeconds );
    
    }
}