﻿namespace OrderBookExample
{
    public class BidAsk
    {
        public decimal AskPrice { get; set; }
        public decimal AskVolume { get; set; }
        public decimal BidPrice { get; set; }
        public decimal BidVolume { get; set; }

        public decimal Price => (BidPrice + AskPrice) / 2;

        public override string ToString()
        {
            return $"{BidPrice}/{BidVolume}-{AskPrice}/{AskVolume}";
        }

        public BidAsk(decimal askPrice, decimal askVolume, decimal bidPrice, decimal bidVolume)
        {
            AskPrice = askPrice;
            AskVolume = askVolume;
            BidPrice = bidPrice;
            BidVolume = bidVolume;
        }
    }
}
