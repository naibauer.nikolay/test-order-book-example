﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderBookExample
{
    public enum Side
    {
        Bid,
        Ask
    }

    public enum Direction
    {
        DESC,
        ASK
    }

    public interface IOrderBook
    {
        // Обновить один уровень ордербука (объем заявок по заданной цене)
        public void Update(Side side, decimal price, decimal size, bool ignoreError = false);

        // заполнить одну сторону ордербука новыми данными
        public void Fill(Side side, IEnumerable<Tuple<decimal, decimal>> data);

        // очистить ордербук. возвращает количество удаленных уровней для Bid и Ask
        public Tuple<int, int> Clear();

        // получить верхний уровень ордербука -- лучшую цену и объем для бидов и асков
        public BidAsk GetBidAsk();

        // получить count верхних уровней одной стороны ордербука.
        // cumulative -- считать кумулятивные объемы
        public Level[] GetTop(Side side, int count, bool cumulative = false);

        // получить несколько верхних уровней ордербука, вплоть до цены price включительно
        // cumulative -- считать кумулятивные объемы
        public Level[] GetTop(Side side, decimal price, bool cumulative = false);

        // получить цену с уровня, где кумулятивный объем превышает cumul
        public decimal? GetPriceWhenCumulGreater(Side side, decimal cumul);

        // возвращает true, если ордербук пуст
        public bool IsEmpty();

    }

    public class OrderBookBase
    {
        private decimal priceMultiplier;
        private decimal sizeMultiplier;

        protected OrderBookBase() { }

        // pricePrecision -- сколько цифр после запятой в ценах
        // sizePrecision -- сколько цифр после запятой в объемах
        public OrderBookBase(uint pricePrecision, uint sizePrecision)
        {
            priceMultiplier = (decimal)Math.Pow(10, -pricePrecision);
            sizeMultiplier = (decimal)Math.Pow(10, -sizePrecision);
        }
    }

    public class OrderBook : OrderBookBase, IOrderBook
    {
        private int askCounter = 0;
        private int bidCounter = 0;

        private int max_amount = 2000;
        private int max_buffer = 2500;

        public Level[] AskBook = new Level[3000];
        public Level[] BidBook = new Level[3000];

        public int AskCounter { get => askCounter; }
        public int BidCounter { get => bidCounter; }

        public void Update(Side side, decimal price, decimal size, bool ignoreError = false)
        {
            if (size >= 0 && price > 0)
                if (side == Side.Ask)
                {
                    UpdateBook(ref AskBook, ref askCounter, price, size, side);
                }
                else
                {
                    UpdateBook(ref BidBook, ref bidCounter, price, size, side);
                }
        }

        public int findIndex(Level[] array, int total_row, Direction dir, decimal target, TypeVal compareBy)
        {
            if (total_row == 0) return 0;

            if ((target > array[total_row - 1].getValue(compareBy) && dir == Direction.ASK) ||
                (target < array[total_row - 1].getValue(compareBy) && dir == Direction.DESC))
            {
                return total_row;
            }


            if ((target == array[total_row - 1].getValue(compareBy) && dir == Direction.ASK) ||
                (target == array[total_row - 1].getValue(compareBy) && dir == Direction.DESC))
            {
                return total_row - 1;
            }

            if ((target <= array[0].getValue(compareBy) && dir == Direction.ASK) ||
              (target >= array[0].getValue(compareBy) && dir == Direction.DESC))
            {
                return 0;
            }

            int low_border = 0;
            int high_border = total_row - 1;
            int middle = 0;
            int target_index = 0;

            for (int i = 0; i < total_row; i++)
            {
                if (high_border - low_border > 1)
                {
                    middle = (high_border + low_border) / 2;
                    if (target == array[middle].getValue(compareBy))
                    {
                        target_index = middle;
                        break;
                    }
                    else if ((target > array[middle].getValue(compareBy) && dir == Direction.ASK) ||
                            (target < array[middle].getValue(compareBy) && dir == Direction.DESC))
                    {
                        low_border = middle;
                    }
                    else
                    {
                        high_border = middle;
                    }
                }
                else
                {
                    target_index = low_border + 1;
                    break;
                }
            }

            return target_index;

        }

        private void UpdateBook(ref Level[] targetBook, ref int total_row, decimal price, decimal size, Side side)
        {
            decimal delta = 0;
            int target_index = findIndex(targetBook, total_row, side == Side.Ask ? Direction.DESC : Direction.ASK, price, TypeVal.PRICE);
            bool update_cumul = false;

            if (targetBook[target_index].Price == price)
            {
                delta = size - targetBook[target_index].Size;
                update_cumul = size != targetBook[target_index].Size;
                targetBook[target_index].Size = size;
                targetBook[target_index].CumulSize = (target_index < total_row ? targetBook[target_index + 1].getValue(TypeVal.CUMULSIZE) : 0) + size;
            }
            else
            {
                for (int k = total_row; k > target_index; k--) targetBook[k] = targetBook[k - 1];

                decimal cumul = (target_index < total_row ? targetBook[target_index + 1].getValue(TypeVal.CUMULSIZE) : 0) + size;
                delta = size;
                update_cumul = size != 0 ? true : false;
                targetBook[target_index] = new Level(price, size, cumul);
                total_row++;
            }

            if (update_cumul)
               for(int i = 0; i < target_index; i++) targetBook[i].CumulSize += delta;

            if (total_row + 1 >= max_buffer)
            {                
                Array.Copy(targetBook, max_buffer - max_amount-1, targetBook, 0, max_amount);
                total_row = max_amount;
            }

        }

        public void Fill(Side side, IEnumerable<Tuple<decimal, decimal>> data)
        {
            foreach (var item in data) Update(side, item.Item1, item.Item2);
        }

        public Tuple<int, int> Clear()
        {
            Array.Clear(AskBook, 0, askCounter);
            Array.Clear(BidBook, 0, bidCounter);
            int a = askCounter;
            int b = bidCounter;
            askCounter = 0;
            bidCounter = 0;
            return Tuple.Create(a, b);
        }

        public BidAsk GetBidAsk()
        {
            bool hasAsk = askCounter > 0;
            bool hasBid = bidCounter > 0;
            return new BidAsk(hasAsk ? AskBook[askCounter - 1].Price : 0,
                              hasAsk ? AskBook[askCounter - 1].Size : 0,
                              hasBid ? BidBook[bidCounter - 1].Price : 0,
                              hasBid ? BidBook[bidCounter - 1].Size : 0);
        }

        public Level[] GetTop(Side side, int count, bool cumulative = false)
        {
            int total = side == Side.Ask ? askCounter : bidCounter;
            count = count > total ? total : count;

            if (count < 0)
                throw new ArgumentOutOfRangeException(nameof(count), "Can't be negative");

            var target = new Level[count];
            Array.Copy(side == Side.Ask ? AskBook : BidBook, total - count, target, 0, count);
            return target;
        }

        public Level[] GetTop(Side side, decimal price, bool cumulative = false)
        {
            int index = findIndex(side == Side.Ask ? AskBook : BidBook,
                side == Side.Ask ? askCounter : bidCounter,
                side == Side.Ask ? Direction.DESC : Direction.ASK,
                price,
                TypeVal.PRICE
                );

            int count = side == Side.Ask ? askCounter - index : bidCounter - index;
            return GetTop(side, count);
        }

        public decimal? GetPriceWhenCumulGreater(Side side, decimal cumul)
        {
            int index = findIndex(side == Side.Ask ? AskBook : BidBook,
               side == Side.Ask ? askCounter : bidCounter,
               Direction.DESC,
               cumul,
               TypeVal.CUMULSIZE
               );

            if (index - 1 < 0) return null;

            return side == Side.Ask ? AskBook[index - 1].Price : BidBook[index - 1].Price;
        }

        public bool IsEmpty()
        {
            return askCounter == 0 && bidCounter == 0;
        }
        public OrderBook(uint pricePrecision, uint sizePrecision) : base(pricePrecision, sizePrecision)
        {
        }
    }
}