﻿namespace OrderBookExample
{
    public enum TypeVal
    {
        PRICE,
        CUMULSIZE
    }

    public struct Level
    {
        // Цена на данной позиции.
        public decimal Price;

        // Объем на данной позиции.
        public decimal Size;

        // Кумулятивный объем на данной позиции.
        public decimal? CumulSize;

        public decimal getValue(TypeVal tVal)
        {
            if (tVal == TypeVal.PRICE)
            {
                return Price;
            }
            else if (tVal == TypeVal.CUMULSIZE)
            {
                return CumulSize == null ? 0 : (decimal)CumulSize;
            }
            else
            {

                return 0;
            }
        }
        public Level()
        {
            Price = 0;
            Size = 0;
            CumulSize = 0;
        }

        public Level(decimal price, decimal size) : this()
        {
            Price = price;
            Size = size;
        }

        public Level(decimal price, decimal size, decimal cumulSize) : this(price, size)
        {
            CumulSize = cumulSize;
        }

        public override string ToString()
        {
            return $"{Price}/{Size}/{CumulSize}";
        }
    }
}
