using NUnit.Framework;
using OrderBookExample;
using System;

namespace UnitTestOrderBook
{
    public class Tests_Update
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestBidBook()
        {
            OrderBook orderBook = new OrderBook(6, 6);
            orderBook.Update(Side.Bid, 0.000004M, 100);
            orderBook.Update(Side.Bid, 0.000304M, 330);
            orderBook.Update(Side.Bid, 0.000504M, 10);
            orderBook.Update(Side.Bid, 0.000007M, 450);
            orderBook.Update(Side.Bid, 0.000008M, 150);
            orderBook.Update(Side.Bid, 0.000044M, 50);
            orderBook.Update(Side.Bid, 0.001044M, 30);
            orderBook.Update(Side.Bid, 0.000504M, 310);
            orderBook.Update(Side.Bid, 0.001044M, 8);
            orderBook.Update(Side.Bid, 0.005044M, 30);
            orderBook.Update(Side.Bid, 0.009044M, 5);
            orderBook.Update(Side.Bid, 0.000504M, 14);
            orderBook.Update(Side.Bid, 0.000014M, 109);
            orderBook.Update(Side.Bid, 0.000094M, 30);

            bool total_num = 11 == orderBook.BidCounter;
            bool total_sum = orderBook.BidBook[0].CumulSize == 1276;

            for (int i = 0; i < orderBook.BidCounter - 1; i++)
                if (orderBook.BidBook[i].Price >= orderBook.BidBook[i + 1].Price) Assert.Fail("Wrong sort order.");

            Assert.AreEqual(true, total_sum && total_num);

            /*
                price 0,000004 size 100 cumul 1276
                price 0,000007 size 450 cumul 1176
                price 0,000008 size 150 cumul 726
                price 0,000014 size 109 cumul 576
                price 0,000044 size 50 cumul 467
                price 0,000094 size 30 cumul 417
                price 0,000304 size 330 cumul 387
                price 0,000504 size 14 cumul 57
                price 0,001044 size 8 cumul 43
                price 0,005044 size 30 cumul 35
                price 0,009044 size 5 cumul 5
            */
        }

        [Test]
        public void TestBookWrongPram()
        {
            try
            {
                OrderBook orderBook = new OrderBook(2, 2);
                orderBook.Update(Side.Bid, -1, -1);
                orderBook.Update(Side.Ask, -1, -1);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [Test]
        public void TestAskBook()
        {
            OrderBook orderBook = new OrderBook(6, 6);
            orderBook.Update(Side.Ask, 0.000004M, 100);
            orderBook.Update(Side.Ask, 0.000304M, 330);
            orderBook.Update(Side.Ask, 0.000504M, 10);
            orderBook.Update(Side.Ask, 0.000007M, 450);
            orderBook.Update(Side.Ask, 0.000008M, 150);
            orderBook.Update(Side.Ask, 0.000044M, 50);
            orderBook.Update(Side.Ask, 0.001044M, 30);
            orderBook.Update(Side.Ask, 0.000504M, 310);
            orderBook.Update(Side.Ask, 0.001044M, 8);
            orderBook.Update(Side.Ask, 0.005044M, 30);
            orderBook.Update(Side.Ask, 0.009044M, 5);
            orderBook.Update(Side.Ask, 0.000504M, 14);
            orderBook.Update(Side.Ask, 0.000014M, 109);
            orderBook.Update(Side.Ask, 0.000094M, 30);

            bool total_num = 11 == orderBook.AskCounter;
            bool total_sum = orderBook.AskBook[0].CumulSize == 1276;

            for (int i = 0; i < orderBook.AskCounter - 1; i++)
                if (orderBook.AskBook[i].Price <= orderBook.AskBook[i + 1].Price) Assert.Fail("Wrong sort order.");

            Assert.AreEqual(true, total_sum && total_num);

            /*
                price 0,009044 size 5 cumul 1276
                price 0,005044 size 30 cumul 1271
                price 0,001044 size 8 cumul 1241
                price 0,000504 size 14 cumul 1233
                price 0,000304 size 330 cumul 1219
                price 0,000094 size 30 cumul 889
                price 0,000044 size 50 cumul 859
                price 0,000014 size 109 cumul 809
                price 0,000008 size 150 cumul 700
                price 0,000007 size 450 cumul 550
                price 0,000004 size 100 cumul 100
            */
        }

        [Test]
        public void TestGetPriceWhenCumulGreater()
        {
            OrderBook orderBook = new OrderBook(6, 6);

            decimal? price_cum = orderBook.GetPriceWhenCumulGreater(Side.Bid, 0);

            Assert.AreEqual(true, price_cum == null);

            orderBook.Update(Side.Bid, 0.000004M, 100);
            orderBook.Update(Side.Bid, 0.000304M, 330);
            orderBook.Update(Side.Bid, 0.000504M, 10);
            orderBook.Update(Side.Bid, 0.000007M, 450);
            orderBook.Update(Side.Bid, 0.000008M, 150);
            orderBook.Update(Side.Bid, 0.000044M, 50);
            orderBook.Update(Side.Bid, 0.001044M, 30);
            orderBook.Update(Side.Bid, 0.000504M, 310);
            orderBook.Update(Side.Bid, 0.001044M, 8);
            orderBook.Update(Side.Bid, 0.005044M, 30);
            orderBook.Update(Side.Bid, 0.009044M, 5);
            orderBook.Update(Side.Bid, 0.000504M, 14);
            orderBook.Update(Side.Bid, 0.000014M, 109);
            orderBook.Update(Side.Bid, 0.000094M, 30);

            price_cum = orderBook.GetPriceWhenCumulGreater(Side.Bid, 387);

            Assert.AreEqual(true, price_cum == 0.000094M);

            price_cum = orderBook.GetPriceWhenCumulGreater(Side.Bid, 1);

            Assert.AreEqual(true, price_cum == 0.009044M);
        }

        [Test]
        public void TestGetTop()
        {
            OrderBook orderBook = new OrderBook(6, 6);

            orderBook.Update(Side.Bid, 0.000004M, 100);
            orderBook.Update(Side.Bid, 0.000304M, 330);
            orderBook.Update(Side.Bid, 0.000504M, 10);
            orderBook.Update(Side.Bid, 0.000007M, 450);
            orderBook.Update(Side.Bid, 0.000008M, 150);
            orderBook.Update(Side.Bid, 0.000044M, 50);
            orderBook.Update(Side.Bid, 0.001044M, 30);
            orderBook.Update(Side.Bid, 0.000504M, 310);
            orderBook.Update(Side.Bid, 0.001044M, 8);
            orderBook.Update(Side.Bid, 0.005044M, 30);
            orderBook.Update(Side.Bid, 0.009044M, 5);
            orderBook.Update(Side.Bid, 0.000504M, 14);
            orderBook.Update(Side.Bid, 0.000014M, 109);
            orderBook.Update(Side.Bid, 0.000094M, 30);

            Level[] topLevels = orderBook.GetTop(Side.Bid, 0.000304M);

            try
            {
                Level[] topLevels_2 = orderBook.GetTop(Side.Bid, 100);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

            Assert.AreEqual(true, 5 == topLevels.Length);
        }

        [Test]
        public void TestGetBidAsk()
        {
            OrderBook orderBook = new OrderBook(6, 6);
            try
            {              
                orderBook.GetBidAsk();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

            orderBook.Update(Side.Ask, 0.000004M, 100);
            orderBook.Update(Side.Ask, 0.000304M, 330);
            orderBook.Update(Side.Ask, 0.000504M, 10);
            orderBook.Update(Side.Ask, 0.000007M, 450);
            orderBook.Update(Side.Ask, 0.000008M, 150);
            orderBook.Update(Side.Ask, 0.000044M, 50);
            orderBook.Update(Side.Ask, 0.001044M, 30);
            orderBook.Update(Side.Ask, 0.000504M, 310);
            orderBook.Update(Side.Ask, 0.001044M, 8);
            orderBook.Update(Side.Ask, 0.005044M, 30);
            orderBook.Update(Side.Ask, 0.009044M, 5);
            orderBook.Update(Side.Ask, 0.000504M, 14);
            orderBook.Update(Side.Ask, 0.000014M, 109);
            orderBook.Update(Side.Ask, 0.000094M, 30);

            try
            {
                BidAsk top_val = orderBook.GetBidAsk();

                if (top_val.AskPrice != 0.000004M || top_val.AskVolume != 100)
                    Assert.Fail("Wrong top ask.");
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

            orderBook.Clear();


            orderBook.Update(Side.Bid, 0.000004M, 100);
            orderBook.Update(Side.Bid, 0.000304M, 330);
            orderBook.Update(Side.Bid, 0.000504M, 10);
            orderBook.Update(Side.Bid, 0.000007M, 450);
            orderBook.Update(Side.Bid, 0.000008M, 150);
            orderBook.Update(Side.Bid, 0.000044M, 50);
            orderBook.Update(Side.Bid, 0.001044M, 30);
            orderBook.Update(Side.Bid, 0.000504M, 310);
            orderBook.Update(Side.Bid, 0.001044M, 8);
            orderBook.Update(Side.Bid, 0.005044M, 30);
            orderBook.Update(Side.Bid, 0.009044M, 5);
            orderBook.Update(Side.Bid, 0.000504M, 14);
            orderBook.Update(Side.Bid, 0.000014M, 109);
            orderBook.Update(Side.Bid, 0.000094M, 30);

            try
            {
                BidAsk top_val = orderBook.GetBidAsk();

                if (top_val.BidPrice != 0.009044M || top_val.BidVolume != 5)
                    Assert.Fail("Wrong top bid.");
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

    }
}